'use strict'

class Employee {
   constructor(name, age, salary){
      this._name = name;
      this._age = age;
      this._salary = salary;
   }

   get name() {
      return this._name;
   }
   set name (name){
      this._name = name;
   }
   
   get age() {
      return this._age;
   }

   set age (age){
      this._age = age;
   }
   
   get salary () {
      return this._salary;
   }

   set salary (salary) {
      this._salary = salary;
   }
};

class Programmer extends Employee{
   constructor(name, age, salary, lang){
      super (name, age, salary)
      this.lang = lang;
   }
   get salary () {
      return this._salary * 3;
   }

   set salary (salary) {
      this._salary = salary;
   }
   
}

const ProgrammerPeter = new Programmer('Peter', 35, 3000, 'en');
const ProgrammerAnna = new Programmer('Anna', 33, 5000, 'en');


console.log(ProgrammerPeter);
console.log(ProgrammerAnna);
