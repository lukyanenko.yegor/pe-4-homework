'use strict'

const math = function () {
   let firstNumber = +prompt('Enter first number');
   let secondNumber = +prompt('Enter second number');
   let mathSymbol = prompt('Enter math symbol');
   if (mathSymbol === '+') {
       return (firstNumber + secondNumber)
   }
   if (mathSymbol === '-') {
       return (firstNumber - secondNumber)
   }
   if (mathSymbol === '*') {
       return (firstNumber * secondNumber)
   }
   if (mathSymbol === '/') {
       return (firstNumber / secondNumber)
   }
}
const res = math()
console.log(res);