'use strict'

const input = document.createElement('input');
const span = document.createElement('span');
const button = document.createElement('button');

input.placeholder = 'Price';
document.body.append(input);

const focusInput = () => {
   input.style.outline = '2px solid green';
   input.style.color = '';
   // span.remove();
   // button.remove();
};

const blurInput = () => {
   
   input.style.outline = '';
   
   if (input.value > 0) {
      document.body.before(span, button);
      span.textContent = `Текущая цена: ${input.value}`;
      button.innerHTML = '&times;';
      input.style.color = 'green';
   }
   
   if (input.value < 0 || 
      isNaN(input.value)) {
      button.remove();
      input.style.outline = '2px solid red';
      document.body.after(span);
      span.textContent = `Please enter correct price`;
      span.style.marginLeft = '10px';
      input.value = '';
   } 
  
};

const delSpan = () => {
   span.remove();
   button.remove();
   input.value = '';
}

input.addEventListener("focus", focusInput);
input.addEventListener("blur", blurInput);
button.addEventListener("click", delSpan);


