'use trict'

const logo = document.querySelector('.logo-title');
const btn = document.querySelector('.btn');
const link = document.querySelectorAll('.contact-link');
const title = document.querySelectorAll('.content-item-title');
const bottom = document.querySelector('.page-bottom-description');

window.addEventListener('load', ()=> {
   if (localStorage.getItem('theme') !== null) {
      let theme = localStorage.getItem('theme');
      logo.style.color = theme;
      btn.style.color = theme;
      link.forEach((e) => {
         e.style.color = theme;
      })
      title.forEach((e) => {
         e.style.color = theme;
      })
      bottom.style.color = theme;
   }
   
   btn.addEventListener('click', ()=> {
      if(btn.style.color == 'white') {
         logo.style.color = 'yellow';
         btn.style.color = 'yellow';
         link.forEach((e) => {
            e.style.color = 'yellow';
         })
         title.forEach((e) => {
            e.style.color = 'yellow';
         })
         bottom.style.color = 'yellow';
         localStorage.setItem('theme', 'yellow');
      } else {
         logo.style.color = 'white';
         btn.style.color = 'white';
         link.forEach((e) => {
            e.style.color = 'white';
         })
         title.forEach((e) => {
            e.style.color = 'white';
         })
         bottom.style.color = 'white';
         localStorage.setItem('theme', 'white');
      }
   });
});


// window.addEventListener('load', ()=> {
//    if(localStorage.getItem('color') !== null) {
//       let theme = localStorage.getItem('color');
//       logo.style.color = theme;
//       btn.style.color = theme;
//       link.forEach((e) => {
//          e.style.color = theme;
//       })
//       title.forEach((e) => {
//          e.style.color = theme;
//       })
//       bottom.style.color = theme;
//    }

//    btn.addEventListener('click', ()=> {
//       if ( btn.style.color === '#FFFFFF') {
//          logo.style.color = '#FFFF00';
//          btn.style.color = '#FFFF00';
//          link.forEach((e) => {
//          e.style.color = '#FFFF00';
//          })
//          title.forEach((e) => {
//          e.style.color = '#FFFF00';
//          })
//          bottom.style.color = '#FFFF00';
//          localStorage.setItem('color', '#FFFF00');
//       } else {
//          logo.style.color = '#FFFFFF';
//          btn.style.color = '#FFFFFF';
//          link.forEach((e) => {
//          e.style.color = '#FFFFFF';
//          })
//          title.forEach((e) => {
//          e.style.color = '#FFFFFF';
//          })
//          bottom.style.color = '#FFFFFF';
//          localStorage.setItem('color', '#FFFFFF');
//       }
//    })
// })