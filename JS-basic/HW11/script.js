'use strict'

const btn = document.querySelectorAll('.btn');

function highlightPressKey() {
   window.addEventListener('keydown', (e) => {
      
      btn.forEach(el => {
         el.style.backgroundColor = '#000000';
         if (el.textContent.toLowerCase() === e.key ||
             el.textContent === e.key) {
             el.style.backgroundColor = 'blue';
         };
      });
   });
   
   
};

highlightPressKey();