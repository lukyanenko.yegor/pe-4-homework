'use strict'

const filterBy = function (arr, type) {
   return arr.filter(item => (typeof item !== type));
 };
 
 console.log(filterBy(['hello', 'world', 23, '23', 45, {}, null], 'object'));