'use strict'

const password = document.querySelector('#password');
const confirmPassword = document.querySelector('#confirm-password');
// const i2 = document.querySelector('i2');
const input = document.querySelector('.password-input');
const confirmInput = document.querySelector('.confirm-password-input');
const button = document.querySelector('button');
const wrong =  document.createElement('span');

function showPassword() {
   


   password.addEventListener('click', () => {
      wrong.remove();
      if(input.type === 'password') {
         input.setAttribute('type', 'text');
         password.classList.add('fa-eye-slash');
         password.classList.remove('fa-eye')
      } else {
         input.setAttribute('type', 'password')
         password.classList.add('fa-eye')
         password.classList.remove('fa-eye-slash');
      }

   })

   confirmPassword.addEventListener('click', () => {
      wrong.remove();
      if(confirmInput.type === 'password') {
         confirmInput.setAttribute('type', 'text');
         confirmPassword.classList.add('fa-eye-slash');
         confirmPassword.classList.remove('fa-eye')
      } else {
         confirmInput.setAttribute('type', 'password')
         confirmPassword.classList.add('fa-eye')
         confirmPassword.classList.remove('fa-eye-slash');
      }

   })

   button.addEventListener('click', () => {
      
      if (input.value === confirmInput.value &&
          input.value !== '') {
         
         alert('You are welcome')
      } else {
         
         wrong.textContent = 'Нужно ввести одинаковые значения';
         wrong.style.color = 'red';
         confirmPassword.insertAdjacentElement("afterend", wrong);
      }
   })
}

showPassword()