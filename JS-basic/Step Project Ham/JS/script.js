'use strict'

const filterItem = document.querySelectorAll('.work-filter-item');
const filter = document.querySelector('.work-filter');
const filterTitle = document.querySelectorAll('.work-filter-title');
const btnLoad = document.querySelector('.btn-load');

filter.addEventListener('click', event=> {

   if (event.target.tagName !== 'LI') return false;
   
   filterTitle.forEach(el => el.classList.remove('active'))
   event.target.classList.add('active')

   let filterClass = event.target.dataset['filter'];
   
   filterItem.forEach( elem => {
      elem.classList.remove('hide-title')
      if (!elem.classList.contains(filterClass) && filterClass !== 'all') {
         elem.classList.add('hide-title')
      }
   })

});

btnLoad.addEventListener('click', ()=> {
   filterItem.forEach(el => el.classList.remove('hide'));
   btnLoad.classList.add('hide')
})





const tab = function () {
   let tabTitle = document.querySelectorAll('.services-tabs-title'); 
   let tabsContent = document.querySelectorAll('.services-tabs-item');
   let tabName;

   tabTitle.forEach(item => {
      item.addEventListener('click', choiseTabTitle)
   });

   function choiseTabTitle() {
      tabTitle.forEach(item => {
         item.classList.remove('active');
      });
      // console.log(this);
      this.classList.add('active');
      tabName = this.getAttribute('data-tabs-name');
      choiseTabsContent(tabName)
   }

   function choiseTabsContent(tabName) {
      tabsContent.forEach(item => {
         item.classList.contains(tabName)? item.classList.add('active'): item.classList.remove('active');
      })
   }
};


tab();

$(document).ready(function(){
   $(".owl-carousel").owlCarousel({
      items: 1,
      loop: true,
      startPosition: 2,
      nav: true,
      dots: true,
   });
 });

