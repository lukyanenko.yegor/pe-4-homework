## 1. Опишите своими словами разницу между функциями `setTimeout()` и `setInterval()`.

Функция `setTimeout()` позволяет вызывать другою функцию единожды, через указанный промежуток времени, а `setInterval()` вызывает функцию безконечное количество раз, повторяя вызов через указанный промежуток времени.


## 2. Что произойдет, если в функцию `setTimeout()` передать нулевую задержку? Сработает ли она мгновенно, и почему?

Если в функцию `setTimeout()` передать нулевую задержку она сработает только после выполнения всего кода на странице. Потому, что она будет находиться в планировщике.


## 3. Почему важно не забывать вызывать функцию `clearInterval()`, когда ранее созданный цикл запуска вам уже не нужен?

Потому, что функция остаеться в памати пока не будет вызван `clearInterval()`. Она может обращаться к внешным переменным, которые могут занимать много памяти. Поэтому, если функция больше не нужна её отменить с помощью `clearInterval()`.