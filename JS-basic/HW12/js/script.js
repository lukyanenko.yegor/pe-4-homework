'use strict'

const images = ['1.jpg', '2.jpg', '3.JPG', '4.png'];
const img = document.querySelector('img');

let timer;
let i = 0;

showImg();
function showImg() {
   img.src = '../images/' + images[i];
   i++;
   if (i == images.length){
      i = 0;
   }
   timer = setTimeout(showImg, 3000)
};

const stop = document.querySelector('.stop');
stop.addEventListener('click', () => {
  clearTimeout(timer)
});

const play = document.querySelector('.play');
play.addEventListener('click', () => {
   setTimeout(showImg, 3000)
});