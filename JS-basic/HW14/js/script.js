'use strict'

$('a.navigation-link').click(function () {
   $('html, body').animate({
      scrollTop: $($(this).attr('href')).offset().top + 'px'
   }, {
      duration: 1000
   })
   return false;
})

$(window).on('scroll', function () {
   if($(window).scrollTop() > document.documentElement.clientHeight) {
      $('.up-top').fadeIn();
   } else {
      $('.up-top').fadeOut();
   };
});

$('.up-top').on('click', function () {
   $('html, body').animate({scrollTop:0}, 'slow');
});

$('.btn-toggle').click(function(){
   $('div#toggle').slideToggle();
})