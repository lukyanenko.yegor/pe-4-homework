'use strict'

const createNewUser = function () {
   const userFirstName = prompt('Enter your firstName');
   const userLastName = prompt('Enter your lastName');
   const userBirthday = prompt('Enter your age, as in the example (dd.mm.yyyy)')
   const newUser = {
      firstName : userFirstName,
      lastName : userLastName,
      birthday: userBirthday,
      getAge(){
         const formatUserBirthday =  new Date(Date.parse(userBirthday.split('.').reverse().join('-')));
         const age = (parseInt((Date.now() - formatUserBirthday) / (1000 * 3600 * 24 * 364.3)));
         return age;
      },
      getPassword(){
         let userPassword = this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.toString().slice(6)
         return userPassword;
      },
   }
   return newUser
};

const user = createNewUser()

console.log(user.getPassword());
console.log(user.getAge());

   





