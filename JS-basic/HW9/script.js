'use strict'

const tab = function () {
   let tabTitle = document.querySelectorAll('.tabs-title'); 
   let tabsContent = document.querySelectorAll('.tab__item');
   let tabName;

   tabTitle.forEach(item => {
      item.addEventListener('click', choiseTabTitle)
   });

   function choiseTabTitle() {
      tabTitle.forEach(item => {
         item.classList.remove('active');
      });
      // console.log(this);
      this.classList.add('active');
      tabName = this.getAttribute('data-tab-name');
      choiseTabsContent(tabName)
   }

   function choiseTabsContent(tabName) {
      tabsContent.forEach(item => {
         item.classList.contains(tabName)? item.classList.add('active'): item.classList.remove('active');
      })
   }
};


tab();